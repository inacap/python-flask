from flask import Blueprint

api = Blueprint("api", __name__)
__import__("api.usuarios")
__import__("api.habitaciones")
__import__("api.pasajeros")
__import__("api.reservas")
