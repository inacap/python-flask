from . import api
from models.clases import Pasajero, Usuario
from models.clases import db
from flask import request
from flask_jwt_extended import jwt_required, get_jwt_identity

@api.get("/pasajeros")
@jwt_required()
def listarPasajeros():
    # pedir los datos del usuario que esta solicitando
    # los datos de los pasajeros
    usuarioActual = get_jwt_identity()
    usuario = db.session.query(Usuario).filter(Usuario.idUsuarios == usuarioActual).one()

    print("Solicitud hecha por usuario:", usuario.username)
    pasajeros = db.session.query(Pasajero).all()
    return [p.toJSON() for p in pasajeros]

@api.post("/pasajeros")
def crearPasajero():
    nombre = request.json.get("nombre")
    apellido = request.json.get("apellido")
    pasajeroNuevo = Pasajero(nombre = nombre, apellido = apellido)
    db.session.add(pasajeroNuevo)
    db.session.commit()
    return {"status": 0}
