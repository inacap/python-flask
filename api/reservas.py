from . import api
from models.clases import Reserva
from models.clases import db
from flask import request
from datetime import datetime

@api.get("/reservas")
def listarReservas():
    reservas = db.session.query(Reserva).all()
    return [p.toJSON() for p in reservas]

@api.post("/reservas")
def crearReservas():
    idPR = request.form.get("idPasajerosResponsable")
    nuevo = Reserva(
        idPasajerosResponsable = idPR,
        fechaCreacion = datetime.now(),
        fechaReserva = datetime.now(),
    )
    db.session.add(nuevo)
    db.session.commit()
    return {"status": 0}
