from . import api
from flask import request
from models.clases import Usuario
from models.clases import db
from flask_jwt_extended import create_access_token
import bcrypt

@api.post("/usuarios")
def crearUsuario():
    #pedir los datos
    username = request.json.get("username")
    password = request.json.get("password")

    # hashear con bcrypt
    passHash = bcrypt.hashpw(bytes(password.encode("utf-8")), bcrypt.gensalt(14))
    nuevoUsuario = Usuario(username = username, password = passHash)
    db.session.add(nuevoUsuario)
    db.session.commit()

    return {"status": 0}

@api.post("/usuarios/login")
def login():
    #pedir los datos
    username = request.json.get("username")
    password = request.json.get("password")

    usuarios = db.session.query(Usuario).filter(Usuario.username == username).all()
    if len(usuarios) == 0:
        # no se encontro usuario
        return {"status": 1}, 401
    else:
        # comparara contraseñas
        hashGuardado = usuarios[0].password
        hashGuardado = bytes(hashGuardado.encode("utf-8"))

        passHash = bcrypt.hashpw(bytes(password.encode("utf-8")), hashGuardado)
        if hashGuardado == passHash:
            # la contraseña coincide, todo ok
            # generar el token de acceso con JWT
            access_token = create_access_token(identity = usuarios[0].idUsuarios)
            return {"status": 0, "access_token": access_token}, 200
        else:
            # la contraseña no coincide
            return {"status": 1}, 401







#
