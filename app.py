from flask import Flask
from markupsafe import escape
from flask import request
from flask import render_template
from flask import redirect
from api import api
from models.clases import db
from flask_jwt_extended import JWTManager
import os

# creamos el servidor
app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = os.environ.get("DB")
app.config["JWT_SECRET_KEY"] = os.environ.get("JWTSK")
jwt = JWTManager(app)
db.init_app(app)

# agrego el Blueprint que creamos dentro del paquete API
app.register_blueprint(api, url_prefix="/api")

# estructura donde almacenare mis datos
listaMascotas = []

# configuracion de nuestro primer endpoint
@app.route("/")
def index():
    return "Web index"

@app.route("/hola/<nombre>")
def hola_mundo(nombre):
    # limpiamos la variable
    nombre = escape(nombre)
    return f"Hola {nombre.capitalize()}!"

@app.get("/mascotas")
def listarMascotas():
    lista = []
    nombre = request.args.get("nombre", None)
    tipo = request.args.get("tipo", None)

    for mascota in listaMascotas:
        seElimina = False
        if nombre and mascota["nombre"] != nombre:
            seElimina = True

        if tipo and mascota["tipo"] != tipo:
            seElimina = True

        if not seElimina:
            lista.append(mascota)
    return lista

@app.post("/mascotas")
def crearMascotas():
    referer = None
    try:
        referer = request.headers.get("Referer")
    except:
        referer = None
    # obtengo los datos de la mascota
    # desde el cuerpo de la solicitud
    nombre = request.form.get("nombre")
    nombre = escape(nombre)

    tipo = request.form.get("tipo")
    tipo = escape(tipo)

    # agrupo los datos
    mascota = {
        "nombre": nombre,
        "tipo": tipo
    }

    listaMascotas.append(mascota)
    # consulto si la mascota se agrego
    # desde la api o desde la pagina
    if referer == None or referer == "API":
        # fue solicitado desde la api
        # respondo con un json
        return {"status": 0}
    else:
        # Redirecciono a la pagina
        return redirect("/vistas/mascotas?estado=1")


@app.route("/vistas/mascotas")
def vistaMascotas():
    # Tomamos el parametro de la url
    estado = request.args.get("estado")
    return render_template("formulario_mascotas.html", mascotas = listaMascotas, estado = escape(estado))
