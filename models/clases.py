from sqlalchemy import String, DateTime, ForeignKey
from sqlalchemy.orm import DeclarativeBase
from sqlalchemy.orm import Mapped
from sqlalchemy.orm import mapped_column
from sqlalchemy.orm import relationship
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime

db = SQLAlchemy()

class Base(DeclarativeBase):
    pass

class Usuario(db.Model):
    __tablename__ = "Usuarios"

    idUsuarios : Mapped[int] = mapped_column(primary_key = True)
    username : Mapped[str] = mapped_column(String(100))
    password : Mapped[str]

    def __repr__(self) -> str:
        return f"Usuario(idUsuarios={self.idUsuarios}, username={self.username})"

    def toJSON(self):
        return {
            "idUsuarios": self.idUsuarios,
            "username": self.username
        }

class Pasajero(db.Model):
    __tablename__ = "Pasajeros"

    idPasajeros : Mapped[int] = mapped_column(primary_key = True)
    nombre : Mapped[str] = mapped_column(String(40))
    apellido : Mapped[str] = mapped_column(String(40))

    def __repr__(self) -> str:
        return f"Pasajero(idPasajeros={self.idPasajeros}, nombre={self.nombre}, apellido={self.apellido})"

    def toJSON(self):
        return {
            "idPasajeros": self.idPasajeros,
            "nombre": self.nombre,
            "apellido": self.apellido
        }

class Reserva(db.Model):
    __tablename__ = "Reservas"

    idReservas : Mapped[int] = mapped_column(primary_key = True)
    fechaCreacion : Mapped[datetime] = mapped_column(DateTime(timezone = True))
    fechaReserva : Mapped[datetime] = mapped_column(DateTime(timezone = True))

    idPasajerosResponsable : Mapped[int] = mapped_column(ForeignKey("Pasajeros.idPasajeros"))
    pasajeroResponsable : Mapped[Pasajero] = relationship("Pasajero")

    def toJSON(self):
        return {
            "idReservas": self.idReservas,
            "fechaCreacion": self.fechaCreacion,
            "fechaReserva": self.fechaReserva,
            "idPasajerosResponsable": self.idPasajerosResponsable,
            "pasajeroResponsable": self.pasajeroResponsable.toJSON()
        }
